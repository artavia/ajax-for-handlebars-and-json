# Description
Fun with Ajax for Handlebars and JSON data.

## Please visit
You can see [the lesson at gitlab.io](https://artavia.gitlab.io/ajax-for-handlebars-and-json/ "the lesson at gitlab") and decide if this is something for you to play around with at a later date.

## What it has
It makes minimal Bootstrap css, asynchronous AJAX calls to each JSON data and one Handlebars template but it is capable of receiving multiple quantities of templates if you choose. Each the data and the template reside in their own respective document independent of the main javascript file. 

## It has been a long time&hellip;
I have not played with templates in a long time. This is because libraries such as **React** and **Angular2+** encourage a shift from template&#45;based layouts to data&#45;driven components. Plus, the ES6 spec has been coming into its own more with features such as **module imports and exports** and **template literals** for starters. So, I felt it was time to address this long ignored piece of &quot;low-hanging fruit.&quot; 

## What is next
In no particular order and after getting acquainted first with each **Angular 6** and likely **AWS**, I would like to revisit **Knockout, EmberJS and Backbone**.

## Comment on project history
This item was originally constructed somewhere between April 2 to April 3, 2014. Then, on July 16, 2014 (or thereabouts), I began to look at helper functions according to my own records.

## My name is Luis
You can call me don Lucho &amp; I hope you have fun during the rest of your day building something cool to share with the rest of the world!