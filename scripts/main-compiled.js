"use strict";

(function () {
  var hbsrequest = false;
  var dataFulfilled = null;
  var asyncHBSTemplate = '';
  var templateName = "pirates";
  var myDataSourceName = "data-".concat(templateName); // const str_host = window.location.host; // ~ 127.0.0.1:8080

  var str_href = window.location.href;
  var ar_new_href = str_href.split('/');
  var ar_hrefLength = ar_new_href.length;
  var displayOut = document.querySelector("#displayOut");

  var loadFunc = function loadFunc() {
    var config = {};
    config.dataFulfilled = dataFulfilled;
    config.dataSourceName = myDataSourceName;
    init(config);
  };

  var init = function init(pm) {
    // console.log( "pm", pm );
    setupTemplates(pm.dataFulfilled);
    fetchTheJsonData(pm.dataSourceName);
    cancelLink(); // urlOps();
  };

  var urlOps = function urlOps() {
    // console.log( "str_host" , str_host ); // 127.0.0.1:8080
    console.log("str_href", str_href);
    console.log("ar_new_href", ar_new_href);
    console.log("ar_hrefLength", ar_hrefLength);
  };

  var cancelLink = function cancelLink() {
    document.querySelector("#figurehead").addEventListener("click", function (evt) {
      evt.preventDefault();
    }, false);
  };

  var setupTemplates = function setupTemplates(fulfilledBool) {
    extractPromiseObjects(fulfilledBool);
    Handlebars.registerHelper("fullName", function (crewMember) {
      return "<strong>".concat(crewMember.firstName, " ").concat(crewMember.lastName, "</strong>");
    });
    Handlebars.registerHelper("allyDeck", function () {
      return "I am a strategic ally to the Mugiwara.";
    });
    Handlebars.registerHelper("UNL_deck", function (text, url) {
      text = this.epithet;
      url = this.blog;
      text = Handlebars.Utils.escapeExpression(text);
      url = Handlebars.Utils.escapeExpression(url);
      var result = "Together with the Mugiwara I have helped defeat powerful enemies and have come to know honorable, powerful friends in the Grand Line. Go see <a href=\"".concat(url, "\" target=\"_blank\">").concat(text, "'s personal bio</a> at the wiki site today.");
      return new Handlebars.SafeString(result);
    });
    Handlebars.registerHelper("normalJob", function (club_Member, occupation) {
      return "My normal job is being the ".concat(occupation, ". I am the ").concat(club_Member, " member of the Mugiwara.");
    });
    Handlebars.registerHelper("IF_crew", function (occupation, th_Member) {
      return "I am the crew ".concat(occupation, " and I am the ").concat(th_Member, " member to have come aboard.");
    });
    Handlebars.registerHelper("bounty_Description", function (epithet, bounty) {
      return "You probably know me as <em>".concat(epithet, "</em> and I carry a bounty of <strong>").concat(bounty, "</strong> Beri.");
    });
    Handlebars.registerHelper("SAFER_url", function (text, url) {
      text = this.epithet;
      url = this.blog;
      text = Handlebars.Utils.escapeExpression(text);
      url = Handlebars.Utils.escapeExpression(url);
      var result = "Visit the wikia and see a <a href=\"".concat(url, "\" target=\"_blank\">").concat(text, "'s bio</a> exclusively today.");
      return new Handlebars.SafeString(result);
    });
    Handlebars.registerHelper("JOLLYROGER", function (ncknm, href) {
      // console.log( "registerHelper::shortBio::this" , this );// borks with FAT ARROW if params are not passed in...
      ncknm = this.epithet;

      if (ar_hrefLength === 4) {
        href = this.jollyRoger.url; // console.log( "href" , href );
      } else if (ar_hrefLength === 5) {
        href = this.jollyRoger.url; // console.log( "href" , href );

        href = href.replace("../", "");
      }

      ncknm = Handlebars.Utils.escapeExpression(ncknm);
      href = Handlebars.Utils.escapeExpression(href);
      var result = " <img src=\"".concat(href, "\" height=\"48\" width=\"48\" alt=\"Picture of one of ").concat(ncknm, "'s flying Jolly Rogers.\"/>");
      return new Handlebars.SafeString(result);
    });
  };

  var extractPromiseObjects = function extractPromiseObjects(pmFulfill) {
    if (pmFulfill === null) {
      pmFulfill = templateGrabber(templateName);
    } // console.log( "pmFulfill" , pmFulfill );

  };

  var templateGrabber = function templateGrabber(templateName) {
    var livehbsrequest = hbsrequest;
    livehbsrequest = new XMLHttpRequest();

    if (!!livehbsrequest) {
      livehbsrequest.onreadystatechange = compileToHBSResponses; // const url = `../templates/${templateName}.handlebars`; 

      var url;

      if (ar_hrefLength === 5) {
        url = "templates/".concat(templateName, ".handlebars");
      } else if (ar_hrefLength === 4) {
        url = "../templates/".concat(templateName, ".handlebars");
      }

      livehbsrequest.overrideMimeType('text/x-handlebars-template');
      livehbsrequest.open("GET", url, true);
      livehbsrequest.onload = neutralizeXHR;
      livehbsrequest.send(null);
    } else {
      alert("An XHR could not be requested.");
    }

    return livehbsrequest;
  };

  var compileToHBSResponses = function compileToHBSResponses(xhr) {
    // function compileToHBSResponses(){
    // console.log( "compileToHBSResponses::xhr" , xhr );
    // console.log( "compileToHBSResponses::xhr.target" , xhr.target );
    // 0 uninitialized - Has not started loading yet
    // 1 loading - The data is loading.
    // 2 loaded - The data has been loaded
    // 3 interactive - A part of the data is available and the user can interact with it
    // 4 complete - Fully loaded... Initialization is ready. 
    if (xhr.target.readyState === 3 && xhr.target.status === 200) {
      var ifBonanza = xhr.target.responseURL.indexOf(templateName) > -1;

      if (ifBonanza === true) {
        asyncHBSTemplate = xhr.target.response; // console.log( "asyncHBSTemplate" , asyncHBSTemplate );

        asyncHBSTemplate = Handlebars.compile(asyncHBSTemplate); // console.log( "asyncHBSTemplate" , asyncHBSTemplate );
      }
    }
  };

  var neutralizeXHR = function neutralizeXHR() {
    hbsrequest = null;
  };

  var fetchTheJsonData = function fetchTheJsonData(name) {
    // let url = `../data/${name}.json`;
    var url;

    if (ar_hrefLength === 5) {
      url = "data/".concat(name, ".json");
    } else if (ar_hrefLength === 4) {
      url = "../data/".concat(name, ".json");
    }

    var request = new XMLHttpRequest();
    request.open("GET", url, true);

    request.onload = function () {
      if (request.status >= 200 && request.status < 400) {
        var data = JSON.parse(request.response);
        createHTML(data);
      } else {
        displayOut.innerHTML = "<p>Connected to server but there was a problem: ".concat(request.statusText, ". </p>"); // console.log(request);
      }
    };

    request.onerror = function () {
      displayOut.innerHTML = "<p>There has been a problem. </p>";
    };

    request.send();
  };

  var createHTML = function createHTML(parsedResults) {
    // console.log( "parsedResults" , parsedResults ); 
    var html = asyncHBSTemplate({
      mugiwara: parsedResults.mugiwara
    }); // console.log( "html" , html ); 

    displayOut.insertAdjacentHTML("beforeend", html); // displayOut.innerHTML = html; 
  };

  window.onload = loadFunc;
})();
