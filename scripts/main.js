( function(){

  let hbsrequest = false;
  let dataFulfilled = null;
  let asyncHBSTemplate = '';
  let templateName = "pirates";
  let myDataSourceName = `data-${templateName}`;

  // const str_host = window.location.host; // ~ 127.0.0.1:8080
  const str_href = window.location.href;
  const ar_new_href = str_href.split('/');
  const ar_hrefLength = ar_new_href.length;

  const displayOut = document.querySelector("#displayOut");
  
  const loadFunc = () => {

    let config = {};
    config.dataFulfilled = dataFulfilled; 
    config.dataSourceName = myDataSourceName;

    init( config );
  };

  const init = ( pm ) => { // console.log( "pm", pm );
    setupTemplates(pm.dataFulfilled);
    fetchTheJsonData( pm.dataSourceName );
    cancelLink();
    // urlOps();
  };

  const urlOps = () => { 
    // console.log( "str_host" , str_host ); // 127.0.0.1:8080
    console.log( "str_href" , str_href ); 
    console.log( "ar_new_href" , ar_new_href ); 
    console.log( "ar_hrefLength" , ar_hrefLength ); 
  };

  const cancelLink = () => {
    document.querySelector("#figurehead").addEventListener( "click", (evt) => {
      evt.preventDefault();
    }, false )
  };

  const setupTemplates = ( fulfilledBool ) => {
    
    extractPromiseObjects( fulfilledBool );

    Handlebars.registerHelper( "fullName" , (crewMember) => { 
      return `<strong>${crewMember.firstName} ${crewMember.lastName}</strong>`;
    } );

    Handlebars.registerHelper( "allyDeck" , function(){ 
      return `I am a strategic ally to the Mugiwara.`;
    } );

    Handlebars.registerHelper( "UNL_deck" , function( text, url){ 
      text = this.epithet;
      url = this.blog;
      text = Handlebars.Utils.escapeExpression(text);
      url = Handlebars.Utils.escapeExpression(url);

      const result = `Together with the Mugiwara I have helped defeat powerful enemies and have come to know honorable, powerful friends in the Grand Line. Go see <a href=\"${url}\" target=\"_blank\">${text}\'s personal bio</a> at the wiki site today.`;

      return new Handlebars.SafeString(result);
    } );

    Handlebars.registerHelper( "normalJob" , (club_Member, occupation ) => { 
      return `My normal job is being the ${occupation}. I am the ${club_Member} member of the Mugiwara.`;
    } );

    Handlebars.registerHelper( "IF_crew" , (occupation, th_Member) => { 
      return `I am the crew ${occupation} and I am the ${th_Member} member to have come aboard.`;
    } );

    Handlebars.registerHelper( "bounty_Description" , (epithet, bounty ) => { 
      return `You probably know me as <em>${epithet}</em> and I carry a bounty of <strong>${bounty}</strong> Beri.`;
    } );

    Handlebars.registerHelper( "SAFER_url" , function(text, url){ 
      text = this.epithet;
      url = this.blog;
      text = Handlebars.Utils.escapeExpression(text);
      url = Handlebars.Utils.escapeExpression(url);
      const result = `Visit the wikia and see a <a href="${url}" target=\"_blank\">${text}\'s bio</a> exclusively today.`;

      return new Handlebars.SafeString(result);
    } );

    Handlebars.registerHelper( "JOLLYROGER" , function(ncknm, href ){ 
      
      // console.log( "registerHelper::shortBio::this" , this );// borks with FAT ARROW if params are not passed in...

      ncknm = this.epithet;
      
      if( ar_hrefLength === 4 ){
        href = this.jollyRoger.url; // console.log( "href" , href );
      }
      else
      if( ar_hrefLength === 5 ){
        href = this.jollyRoger.url; // console.log( "href" , href );
        href = href.replace("../", "");
      }
      
      ncknm = Handlebars.Utils.escapeExpression(ncknm);
      href = Handlebars.Utils.escapeExpression(href);
      
      const result = ` <img src="${href}" height=\"48\" width=\"48\" alt="Picture of one of ${ncknm}\'s flying Jolly Rogers."/>`;

      return new Handlebars.SafeString(result);

    } );

  };

  const extractPromiseObjects = ( pmFulfill ) => {
    if( pmFulfill === null ){
      pmFulfill = templateGrabber( templateName );
    }
    // console.log( "pmFulfill" , pmFulfill );
  };

  const templateGrabber = (templateName) => { 
    
    let livehbsrequest = hbsrequest; 
    livehbsrequest = new XMLHttpRequest();

    if( !!livehbsrequest ){
      livehbsrequest.onreadystatechange = compileToHBSResponses;

      // const url = `../templates/${templateName}.handlebars`; 
      let url;

      if( ar_hrefLength === 5 ){
        url = `templates/${templateName}.handlebars`; 
      }
      else
      if( ar_hrefLength === 4 ){
        url = `../templates/${templateName}.handlebars`; 
      }

      livehbsrequest.overrideMimeType( 'text/x-handlebars-template' );
      livehbsrequest.open( "GET" , url, true ); 
      livehbsrequest.onload = neutralizeXHR;
      livehbsrequest.send( null ); 
    }
    else {
      alert("An XHR could not be requested."); 
    }

    return livehbsrequest;
  };

  const compileToHBSResponses = ( xhr ) => {
    // function compileToHBSResponses(){

    // console.log( "compileToHBSResponses::xhr" , xhr );
    // console.log( "compileToHBSResponses::xhr.target" , xhr.target );

    // 0 uninitialized - Has not started loading yet
    // 1 loading - The data is loading.
    // 2 loaded - The data has been loaded
    // 3 interactive - A part of the data is available and the user can interact with it
    // 4 complete - Fully loaded... Initialization is ready. 
    
    if( (xhr.target.readyState === 3) && (xhr.target.status === 200) ){ 
      var ifBonanza = xhr.target.responseURL.indexOf( templateName ) > -1;
      if( ifBonanza === true ){
        asyncHBSTemplate = xhr.target.response;
        // console.log( "asyncHBSTemplate" , asyncHBSTemplate );
        asyncHBSTemplate = Handlebars.compile( asyncHBSTemplate );
        // console.log( "asyncHBSTemplate" , asyncHBSTemplate );
      }
    }
  };

  const neutralizeXHR = () => { 
    hbsrequest = null;
  };

  const fetchTheJsonData = ( name ) => {

    // let url = `../data/${name}.json`;

    let url;
    
    if( ar_hrefLength === 5 ){
      url = `data/${name}.json`;
    }
    else
    if( ar_hrefLength === 4 ){
      url = `../data/${name}.json`;
    }
    
    const request = new XMLHttpRequest();
    request.open("GET", url, true );
    request.onload = () => {
      if( request.status >= 200 && request.status < 400 ){
        const data = JSON.parse(request.response);
        createHTML(data);
      }
      else {
        displayOut.innerHTML = `<p>Connected to server but there was a problem: ${request.statusText}. </p>`;
        // console.log(request);
      }
    };
    
    request.onerror = () => {
      displayOut.innerHTML = "<p>There has been a problem. </p>";
    };

    request.send();
  };

  const createHTML = ( parsedResults ) => { 
    // console.log( "parsedResults" , parsedResults ); 
    let html = asyncHBSTemplate( { mugiwara: parsedResults.mugiwara } ); 
    // console.log( "html" , html ); 
    
    displayOut.insertAdjacentHTML("beforeend" , html ); // displayOut.innerHTML = html; 
  };

  window.onload = loadFunc;  

} )();
